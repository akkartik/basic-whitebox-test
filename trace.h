#include<iostream>
using std::ostream;
#include <fstream>
#include<sstream>
using std::ostringstream;
#include<string>
using std::string;
#include<vector>
using std::vector;

//// Interface for white-box tests {

// Add to the trace (in production code) {

// Example usage:
//   trace(2, "abc") << "line 1" << end();
//
// This call emits this line to the trace:
//   "2 abc: line 1"
//
// The label is a namespace for assertions.
//
// The depth is an indicator of importance, helpful for hiding irrelevant
// details in tools. It is not used in tests.

#define trace(depth, layer)  !trace_stream \
    ? /*print nothing; all args attach to the other branch*/std::cerr \
    : trace_stream->stream(depth, layer)
// }

// Assertions on the trace (in tests) {

// Check that the trace contains some sequence of lines, in sequence, though
// possibly with other lines mixed in.
// Lines are separated by newlines.
//
// Each line is of the form "label: contents".
//
// Example usage:
// To check for the example above:
//   CHECK_TRACE_CONTENTS("abc: line 1\n")
#define CHECK_TRACE_CONTENTS(...)  check_trace_contents(__FUNCTION__, __FILE__, __LINE__, __VA_ARGS__)

// Check that the trace doesn't contain a single line.
#define CHECK_TRACE_DOESNT_CONTAIN(...)  CHECK(trace_doesnt_contain(__VA_ARGS__))

// Check that a trace contains a fixed number of lines with a given label.
#define CHECK_TRACE_COUNT(label, count) \
  if (Passed && trace_count(label) != (count)) { \
    cerr << "\nF - " << __FUNCTION__ << "(" << __FILE__ << ":" << __LINE__ << "): trace_count of " << label << " should be " << count << '\n'; \
    cerr << "  got " << trace_count(label) << '\n';  /* multiple eval */ \
    cerr << trace_stream->readable_contents(label); \
    Passed = false; \
    return;  /* Currently we stop at the very first failure. */ \
  }
// }

// }

struct TraceLine {
  string contents;
  string label;
  int depth;  // 0 is 'sea level'; positive integers are progressively 'deeper' and lower level
  TraceLine(string c, string l, int d);
};

struct TraceStream {
  vector<TraceLine> past_lines;
  // accumulator for current trace_line
  ostringstream* curr_stream;
  string curr_label;
  int curr_depth;
  // other stuff

  TraceStream();
  // start accumulating to a new trace line
  ostream& stream(int depth, string label);
  // finalize the trace line most recently started
  void newline();
  // extract lines matching a given label
  // empty label matches all lines
  string readable_contents(string label);
};

// Where the trace is accumulated.
extern TraceStream* trace_stream;

// Passing any object of this type to any ostream stops adding to the current
// trace line.
struct end {};
ostream& operator<<(ostream& os, end /*unused*/);

// Trace depths can go from 0 to MAX_DEPTH (both inclusive)
const int MAX_DEPTH = 9999;

// Optional file to dump the trace to. Useful for debugging.
extern std::ofstream trace_file;

bool check_trace_contents(string FUNCTION, string FILE, int LINE, string expected);
bool trace_doesnt_contain(string expected);
int trace_count(string label, string line);
