CFLAGS ?= -g -O3
CFLAGS := ${CFLAGS} -Wall -Wextra -ftrapv -fno-strict-aliasing

a.out: *.cc *.h test_function_list test_list test_name_list
	${CXX} ${CFLAGS} *.cc

test_function_list: *.cc
	grep -h "^void test_.*) {" *.cc  |sed 's/ {.*/;/' > test_function_list

test_list: *.cc
	grep -h "^\s*void test_" *.cc  |sed 's/^\s*void \(.*\)() {.*/\1,/' > test_list

test_name_list: *.cc
	grep -h "^\s*void test_" *.cc  |sed 's/^\s*void \(.*\)() {.*/"\1",/' > test_name_list

.PHONY: clean

clean:
	-rm -rf a.out* test_function_list test_list test_name_list
