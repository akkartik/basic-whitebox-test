#include "test.h"  // we don't define tests here, but we're creating new assertions for tests, sometimes based on existing ones.
#include "trace.h"
#include "trace_proto.h"

#include<iostream>
using std::ostream;
using std::cerr;
#include <iomanip>

std::ofstream trace_file;  // for debugging

// start accumulating to a new trace line
ostream& TraceStream::stream(int depth, string label) {
  curr_stream = new ostringstream;
  curr_label = label;
  curr_depth = depth;
  return *curr_stream;
}

// finalize the trace line most recently started
void TraceStream::newline() {
  string trim(const string& s);
  if (!curr_stream) return;
  string curr_contents = curr_stream->str();
  if (!curr_contents.empty()) {
    past_lines.push_back(TraceLine(curr_contents, trim(curr_label), curr_depth));  // preserve indent in contents
    if (trace_file)
      trace_file << std::setw(4) << curr_depth << ' ' << curr_label << ": " << curr_contents << '\n';
  }

  // clean up
  delete curr_stream;
  curr_stream = NULL;
  curr_label.clear();
  curr_depth = MAX_DEPTH;
}

TraceLine::TraceLine(string c, string l, int d) {
  contents = c;
  label = l;
  depth = d;
}

TraceStream::TraceStream() {
  curr_stream = NULL;
  curr_depth = MAX_DEPTH;
}

TraceStream* trace_stream = NULL;

// some syntax for finalizing trace lines
//  trace(...) << ... << end();
ostream& operator<<(ostream& os, end /*unused*/) {
  if (trace_stream) trace_stream->newline();
  return os;
}

bool check_trace_contents(string FUNCTION, string FILE, int LINE, string expected) {
  if (!Passed) return false;
  if (!trace_stream) return false;
  vector<string> expected_lines = split(expected, "\n");
  int curr_expected_line = 0;
  while (curr_expected_line < expected_lines.size() && expected_lines.at(curr_expected_line).empty())
    ++curr_expected_line;
  if (curr_expected_line == expected_lines.size()) return true;
  string label, contents;
  split_label_contents(expected_lines.at(curr_expected_line), &label, &contents);
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label != p->label) continue;
    if (contents != trim(p->contents)) continue;
    ++curr_expected_line;
    while (curr_expected_line < expected_lines.size() && expected_lines.at(curr_expected_line).empty())
      ++curr_expected_line;
    if (curr_expected_line == expected_lines.size()) return true;
    split_label_contents(expected_lines.at(curr_expected_line), &label, &contents);
  }

  if (line_exists_anywhere(label, contents)) {
    cerr << "\nF - " << FUNCTION << "(" << FILE << ":" << LINE << "): line [" << label << ": " << contents << "] out of order in trace:\n";
    cerr << trace_stream->readable_contents("");
  }
  else {
    cerr << "\nF - " << FUNCTION << "(" << FILE << ":" << LINE << "): missing [" << contents << "] in trace:\n";
    cerr << trace_stream->readable_contents(label);
  }
  Passed = false;
  return false;
}

string TraceStream::readable_contents(string label) {
  string trim(const string& s);  // prototype
  ostringstream output;
  label = trim(label);
  for (vector<TraceLine>::iterator p = past_lines.begin();  p != past_lines.end();  ++p)
    if (label.empty() || label == p->label)
      output << std::setw(4) << p->depth << ' ' << p->label << ": " << p->contents << '\n';
  return output.str();
}

bool trace_doesnt_contain(string expected) {
  vector<string> tmp = split_first(expected, ": ");
  if (tmp.size() == 1) {
    cerr << expected << ": missing label or contents in trace line\n";
    exit(1);
  }
  return trace_count(tmp.at(0), tmp.at(1)) == 0;
}

int trace_count(string label, string line) {
  if (!trace_stream) return 0;
  long result = 0;
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label == p->label) {
      if (line == "" || trim(line) == trim(p->contents))
        ++result;
    }
  }
  return result;
}

int trace_count_prefix(string label, string prefix) {
  if (!trace_stream) return 0;
  long result = 0;
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label == p->label) {
      if (starts_with(trim(p->contents), trim(prefix)))
        ++result;
    }
  }
  return result;
}

void split_label_contents(const string& s, string* label, string* contents) {
  static const string delim(": ");
  size_t pos = s.find(delim);
  if (pos == string::npos) {
    *label = "";
    *contents = trim(s);
  }
  else {
    *label = trim(s.substr(0, pos));
    *contents = trim(s.substr(pos+delim.size()));
  }
}

bool line_exists_anywhere(const string& label, const string& contents) {
  for (vector<TraceLine>::iterator p = trace_stream->past_lines.begin();  p != trace_stream->past_lines.end();  ++p) {
    if (label != p->label) continue;
    if (contents == trim(p->contents)) return true;
  }
  return false;
}

// helpers

// strip whitespace at start and end of a string
string trim(const string& s) {
  string::const_iterator first = s.begin();
  while (first != s.end() && isspace(*first))
    ++first;
  if (first == s.end()) return "";
  string::const_iterator last = --s.end();
  while (last != s.begin() && isspace(*last))
    --last;
  ++last;
  return string(first, last);
}

vector<string> split(string s, string delim) {
  vector<string> result;
  size_t begin=0, end=s.find(delim);
  while (true) {
    if (end == string::npos) {
      result.push_back(string(s, begin, string::npos));
      break;
    }
    result.push_back(string(s, begin, end-begin));
    begin = end+delim.size();
    end = s.find(delim, begin);
  }
  return result;
}

vector<string> split_first(string s, string delim) {
  vector<string> result;
  size_t end=s.find(delim);
  result.push_back(string(s, 0, end));
  if (end != string::npos)
    result.push_back(string(s, end+delim.size(), string::npos));
  return result;
}

bool starts_with(const string& s, const string& pat) {
  string::const_iterator a=s.begin(), b=pat.begin();
  for (/*nada*/;  a!=s.end() && b!=pat.end();  ++a, ++b)
    if (*a != *b) return false;
  return b == pat.end();
}
