// forward declarations for some helpers; not part of the public interface
int trace_count_prefix(string label, string prefix);
void split_label_contents(const string& s, string* label, string* contents);
bool line_exists_anywhere(const string& label, const string& contents);
string trim(const string& s);
vector<string> split(string s, string delim);
vector<string> split_first(string s, string delim);
bool starts_with(const string& s, const string& pat);
