#include<iostream>

#include "test.h"
#include "trace.h"

// Bare-bones program; either:
//   a) run all tests, or
//   b) run a single test while dumping its trace to a file.
int main(int argc, char* argv[]) {
  if (argc == 2) {
    std::cerr << "dumping trace to file 'last_run'\n";
    trace_file.open("last_run", std::ios::out);
    return run_single_test(argv[1]);
  }
  return run_tests();
}

// One possible implementation of run

int run(int x) {
  trace(0, "app") << "transforming " << x << end();
  int g(int);  // prototype
  int y = g(x);
  int z = 2*y;
  trace(0, "app") << x << " transformed to " << z << end();
  return z;
}

int g(int x) {
  int y = x+1;
  trace(1, "app") << x << " + 1 is " << y << end();
  return y;
}

void test_1() {
  run(3);
  CHECK_TRACE_CONTENTS(
      "app: transforming 3\n"
      "app: 3 + 1 is 4\n"
      "app: 3 transformed to 8\n");
}

// A second possible implementation of run. Pretend it's an earlier or later
// version.

int run2(int x) {
  trace(0, "app") << "transforming " << x << end();
  int y = x+1;
  trace(1, "app") << x << " + 1 is " << y << end();
  int z = 2*y;
  trace(0, "app") << x << " transformed to " << z << end();
  return z;
}

void test_2() {
  run2(3);
  CHECK_TRACE_CONTENTS(
      "app: transforming 3\n"
      "app: 3 + 1 is 4\n"
      "app: 3 transformed to 8\n");
}

void reset() {
  if (trace_stream) delete trace_stream;
  trace_stream = new TraceStream;
  // other app-specific test setup will happen here
}
