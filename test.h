// Implement this function to initialize each test, and to clean up at the end.
void reset();

// Write your tests in functions with the precise first line:
//   `void test_xxxxx() {`
typedef void (*test_fn)();

// The build system will auto-generate a list of such functions.
extern const test_fn Tests[];  // convention: global variables are capitalized

// Inside each test, signal failure by setting the global variable 'Passed' to
// false.
extern bool Passed;

// A helper to help signal failure and print out failing tests.
#define CHECK(X) \
  if (Passed && !(X)) { \
    std::cerr << "\nF - " << __FUNCTION__ << "(" << __FILE__ << ":" << __LINE__ << "): " << #X << '\n'; \
    Passed = false; \
    return;  /* stop at the very first failure inside a test */ \
  }

#define CHECK_EQ(X, Y) \
  if (Passed && (X) != (Y)) { \
    std::cerr << "\nF - " << __FUNCTION__ << "(" << __FILE__ << ":" << __LINE__ << "): " << #X << " == " << #Y << '\n'; \
    std::cerr << "  got " << (X) << '\n';  /* BEWARE: multiple eval */ \
    Passed = false; \
    return;  /* Currently we stop at the very first failure. */ \
  }

// To run all your tests, call `run_tests()`, say within main after parsing
// some commandline flag. It will print a dot for each passing test, and more
// verbose messages about failing tests.
int run_tests();

// To run a single test, call `run_single_test()` with a string containing the
// name of the test to run, say from a commandline argument.
int run_single_test(const char*);
